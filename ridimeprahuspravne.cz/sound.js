sound = {
    game: document.createElement('audio'),
    game2: document.createElement('audio'),
    gameOver: document.createElement('audio'),
    button: document.createElement('audio'),
    win: document.createElement('audio'),
    wrong: document.createElement('audio'),
    clang: document.createElement('audio'),
    special: document.createElement('audio'),
    agent: document.createElement('audio'),
};

sound.game.setAttribute('src', 'sound/wind.mp3');
sound.game.load();
sound.game.volume = 0.4;
sound.game.addEventListener('ended', function () {
    this.currentTime = 0;
    this.play();
}, false);

sound.game2.setAttribute('src', 'sound/ship_ambient.mp3');
sound.game2.load();
sound.game2.volume = 0.15;
sound.game2.addEventListener('ended', function () {
    this.currentTime = 0;
    this.play();
}, false);

sound.button.setAttribute('src', 'sound/button.mp3');
sound.button.load();
sound.button.addEventListener('ended', function () {
    this.pause();
}, false);

sound.win.setAttribute('src', 'sound/win.mp3');
sound.win.load();
sound.win.addEventListener('ended', function () {
    this.pause();
}, false);

sound.wrong.setAttribute('src', 'sound/wrong.mp3');
sound.wrong.load();
sound.wrong.volume = 0.25;
sound.wrong.addEventListener('ended', function () {
    this.pause();
}, false);

sound.clang.setAttribute('src', 'sound/clang.mp3');
sound.clang.load();
sound.clang.volume = 0.4;
sound.clang.addEventListener('ended', function () {
    this.pause();
}, false);

sound.special.setAttribute('src', 'sound/ship_full.mp3');
sound.special.load();
sound.special.volume = 0.8;
sound.special.addEventListener('ended', function () {
    this.pause();
}, false);

sound.agent.setAttribute('src', 'sound/agent-initial.mp3');
sound.agent.load();
sound.agent.volume = 1.0;
sound.agent.addEventListener('ended', function () {
    this.pause();
}, false);
