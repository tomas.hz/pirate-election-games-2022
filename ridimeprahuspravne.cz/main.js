$(window).load(function () {
  iface = document.querySelector(".iface");

  const leaderboardURL = "https://ridimeprahuspravne.cz/leaderboard-api/";
  var readyToStart = true; // if the game is ready to start
  var game = null; // var for handling the game object
  var audioOn = true; // if true audio is on
  var voteDiag = false; // true: the pre-win dialog is active
  var voteCheckDiag = false;
  var endTime = 2 * 60; // time when the game ends
  var gameSpeed = 1000; // speed of game tick
  var chosenButton = null;
  var noOfActiveTargets = 10;
  var tutorialTargets = [];
  var tutorialActive = false;
  var tutorial = null;
  var blinkTimer = null;
  var tutorialChosenButton = null;
  var finalScore = 0;

  // game size
  winSize = {
    x: 0,
    y: 0,
  };

  function buttonPressed(keyName) {
    if (audioOn) sound.button.cloneNode(true).play();
    if (tutorialActive) {
      // tutorial button behavior
      moveBgButton(tutorialChosenButton);
      if (
        keyName.localeCompare(tutorialChosenButton.substring(1)) == 0
      ) {
        clearInterval(blinkTimer);
        blinkTimer = null;
        chosenButton = "#"+keyName;
        if (
          keyName.localeCompare("pirati") == 0 &&
          tutorial.specialActive
        ) {
          // special used in tutorial
          showSpecialEffect();
          tutorial.useSpecial();
          chosen();
          setTimeout(function () {
            if (tutorialActive) {
              $(".button_container").fadeOut(500);
              $("#button_bg").fadeOut(500);
              sound.special.pause()
              if (tutorial !== null) {
                tutorial.agentRaid();
                $("#note_special").transition({opacity: 0.0},500);
                $("#note_agent").transition({opacity: 1.0},500);
              }
            }
          }, 5 * 1000);
        }
      } else {
        if (blinkTimer === null) blink(9e8, 1000);
      }
    } else {
      chosen("#"+keyName);
      if (
        $(chosenButton).attr("id").localeCompare("pirati") == 0 &&
        game.specialActive
      ) {
        showSpecialEffect();
        game.useSpecial();
        chosen();
      }
    }
  }

  /** handle pressed button (control and pause) */
  $(document).keydown(function (e) {
    if (game !== null || tutorial !== null) {
      var button = null;
      switch (e.which) {
        case 81:
          //q: lupa
          button = "lupa"
          break;
        case 87:
          //w: priroda
          button = "priroda"
          break;
        case 69:
          //e: bydleni
          button = "bydleni"
          break;
        case 82:
          //r: digitalizace
          button = "digitalizace"
          break;
        case 84:
          //t: energie
          button = "energie"
          break;
        case 90:
          //z: zdravi
          button = "zdravi"
          break;
        case 89:
          //y: zdravi
          button = "zdravi"
          break;
        case 85:
          //u: pirati
          button = "pirati"
          break;
        default:
          return;
      }
      if (button != null) {
        buttonPressed(button);
      }
    }
  });

  function toggleFullScreen(elem) {
    // ## The below if statement seems to work better ## if ((document.fullScreenElement && document.fullScreenElement !== null) || (document.msfullscreenElement && document.msfullscreenElement !== null) || (!document.mozFullScreen && !document.webkitIsFullScreen)) {
    if (
      (document.fullScreenElement !== undefined &&
        document.fullScreenElement === null) ||
      (document.msFullscreenElement !== undefined &&
        document.msFullscreenElement === null) ||
      (document.mozFullScreen !== undefined && !document.mozFullScreen) ||
      (document.webkitIsFullScreen !== undefined &&
        !document.webkitIsFullScreen)
    ) {
      if (elem.requestFullScreen) {
        elem.requestFullScreen();
      } else if (elem.mozRequestFullScreen) {
        elem.mozRequestFullScreen();
      } else if (elem.webkitRequestFullScreen) {
        elem.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
      } else if (elem.msRequestFullscreen) {
        elem.msRequestFullscreen();
      }
      $("#fullscreen_on").hide();
      $("#fullscreen_off").fadeIn(0);
    } else {
      if (document.cancelFullScreen) {
        document.cancelFullScreen();
      } else if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
      } else if (document.webkitCancelFullScreen) {
        document.webkitCancelFullScreen();
      } else if (document.msExitFullscreen) {
        document.msExitFullscreen();
      }
      $("#fullscreen_off").hide();
      $("#fullscreen_on").fadeIn(0);
    }
  }

  /** click on sound button */
  $(".fullscreen_button").click(function () {
    toggleFullScreen(document.body);
  });

  function animHide(obj, duration = 0.2) {
    $(obj).hide();
    // $(obj).qcss({ visibility: "hidden" });
  }

  /**save active action button and move visual effect */
  function chosen(obj = null) {
    chosenButton = obj;
    if (
      chosenButton === null ||
      $(chosenButton).attr("id").localeCompare("pirati") == 0
    ) {
      if (game !== null) {
        if (!game.specialActive) {
          $("#button_bg").css({ visibility: "hidden" });
          $("#button_bg").hide();
        }
      } else if (tutorial !== null) {
        if (!tutorial.specialActive) {
          $("#button_bg").css({ visibility: "hidden" });
          $("#button_bg").hide();
        }
      }
    } else {
      moveBgButton(obj);
    }
  }

  function moveBgButton(obj) {
    $("#button_bg").css({ visibility: "visible" });
    $("#button_bg").fadeIn(0);
    button_pos = $(obj).parent().position();
    button_width = $(obj).parent().width()
    button_bg_width = $("#button_bg").width();
    ratio = 0.5;
    // $("#button_bg").css({ left: button_pos.left - ratio * button_bg_width });
    // $("#button_bg").css({ top: button_pos.top - ratio * button_bg_width });
    $("#button_bg").css({ left: button_pos.left - (button_bg_width-button_width)/2 });
    $("#button_bg").css({ top: button_pos.top - (button_bg_width-button_width)/2});
  }

  function idValue(s = "blank-1") {
    return (((s.substring(s.length - 2) - 1) % 6) + 1).toString();
  }

  /** show effects for special button */
  function showSpecialEffect() {
    duration = 5.0;
    sound.special.currentTime = 0
    sound.special.play();
    if (audioOn) sound.special.volume = 0.8;
    else sound.special.volume = 0.0;
    // $("#effect_special").css({ left: -30 + "%", visibility: "visible" });
    $("#effect_special").fadeIn(500).transition({ left: -30 + "%", visibility: "visible" }, 0)
      .transition({ left: 100 + "%" }, duration * 1000)
      .delay(duration * 1000).fadeOut(500);
    setTimeout(function () {
      sound.special.pause();
    }, duration * 1000);

  }

  /** show effects for standard target */
  function showEffect(id, effect_coors) {
    var duration = 500;
    effectID = "#effect0" + idValue(id);
    $(effectID).css({ visibility: "hidden" });
    width = $(effectID).width();
    height = $(effectID).height();
    $(effectID).css({ visibility: "visible" });
    $(effectID).fadeIn(0);
    $(effectID).css({ opacity: 0.0 });
    $(effectID).css({ left: effect_coors[0] - width / 2 + "px" });
    $(effectID).css({ top: effect_coors[1] - height / 2 + "px" });
    $(effectID).animate(
      {
        top: effect_coors[1] - (1.5 * height) / 2 + "px",
        opacity: 0.8,
      },
      duration
    );
    $(effectID).delay(duration).fadeOut(duration);
    // console.log(effectID, effect_coors[0], effect_coors[1]);
  }

  function animHideAgent(removal, duration) {
    $(removal).fadeOut(duration*1000);
  }

  /** click on target */
  $(".target").click(function (e) {
    removal = this;
    duration = 0.2;
    if (game !== null) {
      if ($(removal).attr("id").localeCompare("target_agent") == 0) {
        // click on agent
        if (audioOn) sound.clang.cloneNode(true).play();
        if (game.tAgent !== null) {
          game.addScore($(removal).attr("id"));
          game.tAgent.lives--;
        } else {
          animeHide(removal, duration);
        }
        if (game.tAgent.lives == 0) {
          animHide(removal, duration);

          setTimeout(function () {
            if (game !== null) game.endAgentRaid();
            else {
              sound.agent.pause();
            }
          }, duration * 1000);
        }
      } else if (game.isValidTarget(removal, chosenButton)) {
        // hit: add score, run animation and remove target from list of active
        if (audioOn) sound.clang.cloneNode(true).play();
        game.addScore($(removal).attr("id"));
        var clickxy = [e.pageX, e.pageY];
        showEffect($(removal).attr("id"), clickxy);
        animHide(removal, duration);

        setTimeout(function () {
          if (game !== null)
            game.removeTarget(removal);
        }, duration * 1000);
      } else {
        // WRONG target!
        if (audioOn) sound.wrong.cloneNode(true).play();
      }
    } else {
      if (tutorialActive) {
        if ($(this).attr("id").localeCompare("target01") == 0) {
          if (tutorial.isValidTarget(removal, chosenButton)) {
            // 1st tutorial target clicked
            $("#note_target01").transition({opacity: 0.0},500);
            if (audioOn) sound.clang.cloneNode(true).play();
            animHide(removal, duration);
            var clickxy = [e.pageX, e.pageY];
            showEffect($(removal).attr("id"), clickxy);
            tutorialChosenButton = "#priroda";
            moveBgButton(tutorialChosenButton);
            if (blinkTimer === null) blink(1e5, 1000);
            tutorialTargets[1].set(targetList[13]);
            $("#note_target02").transition({opacity: 1.0},500);
          } else {
            // WRONG target!
            if (audioOn) sound.wrong.cloneNode(true).play();
          }
        } else if ($(this).attr("id").localeCompare("target14") == 0) {
          if (tutorial.isValidTarget(removal, chosenButton)) {
            // 2nd tutorial target clicked
            $("#note_target02").transition({opacity: 0.0},500);
            if (audioOn) sound.clang.cloneNode(true).play();
            animHide(removal, duration);
            var clickxy = [e.pageX, e.pageY];
            showEffect($(removal).attr("id"), clickxy);
            tutorialChosenButton = "#bydleni";
            moveBgButton(tutorialChosenButton);
            if (blinkTimer === null) blink(1e5, 1000);
            tutorialTargets[2].set(targetList[2]);
            $("#note_target03").transition({opacity: 1.0},500);
          } else {
            // WRONG target!
            if (audioOn) sound.wrong.cloneNode(true).play();
          }
        } else if ($(this).attr("id").localeCompare("target03") == 0) {
          if (tutorial.isValidTarget(removal, chosenButton)) {
            // 3rd tutorial target clicked
            $("#note_target03").transition({opacity: 0.0},500);
            if (audioOn) sound.clang.cloneNode(true).play();
            animHide(removal, duration);
            var clickxy = [e.pageX, e.pageY];
            showEffect($(removal).attr("id"), clickxy);
            for (var i = 0; i < 3; i++) {
              tutorial.randomTarget();
            }
            setTimeout(function () {
              tutorialChosenButton = "#pirati";
              moveBgButton(tutorialChosenButton);
              tutorial.specialActive = true;
              $("#pirati").css({ opacity: 1.0 });
              $("#note_general").transition({opacity: 0.0},500);
              $("#note_special").transition({opacity: 1.0},500);
            }, duration * 1000);
          } else {
            // WRONG target!
            if (audioOn) sound.wrong.cloneNode(true).play();
          }
        } else if ($(this).attr("id").localeCompare("target_agent") == 0) {
          // click on agent
          if (audioOn) sound.clang.cloneNode(true).play();
          tutorial.tAgent.lives--;
          if (tutorial.tAgent.lives == 0) {
            animHide(removal, duration);
            $("#note_agent").fadeOut(500);

            setTimeout(function () {
              if(tutorialActive) {
                if (tutorial !== null) {
                  tutorial.endAgentRaid();
                  endTutorial();
                } else {
                  sound.agent.pause();
                  endTutorial();
                }
              }
            }, 4*duration * 1000);
          }
        }
      }
    }
  });

  /** choose the action button */
  $(".button").click(function () {
    if (audioOn) sound.button.cloneNode(true).play();
    if (tutorialActive) {
      // tutorial button behavior
      moveBgButton(tutorialChosenButton);
      if (
        $(this).attr("id").localeCompare(tutorialChosenButton.substring(1)) == 0
      ) {
        clearInterval(blinkTimer);
        blinkTimer = null;
        chosenButton = this;
        if (
          $(chosenButton).attr("id").localeCompare("pirati") == 0 &&
          tutorial.specialActive
        ) {
          // special used in tutorial
          showSpecialEffect();
          tutorial.useSpecial();
          chosen();
          setTimeout(function () {
            if (tutorialActive) {
              $(".button_container").fadeOut(500);
              $("#button_bg").fadeOut(500);
              sound.special.pause()
              if (tutorial !== null) {
                tutorial.agentRaid();
                $("#note_special").transition({opacity: 0.0},500);
                $("#note_agent").transition({opacity: 1.0},500);
              }
            }
          }, 5 * 1000);
        }
      } else {
        if (blinkTimer === null) blink(9e8, 1000);
      }
    } else {
      chosen(this);
      if (
        $(chosenButton).attr("id").localeCompare("pirati") == 0 &&
        game.specialActive
      ) {
        showSpecialEffect();
        game.useSpecial();
        chosen();
      }
    }
  });

  $("#restart_button").click(function () {
    restartGame();
  });

  /** click on sound button */
  $(".sound_button").click(function () {
    if (audioOn) {
      audioOn = false;
      $.each(sound, function (index, value) {
        value.volume = 0.0;
      });
      $("#sound_on").hide();
      $("#sound_off").fadeIn(0);
    } else {
      audioOn = true;
      $.each(sound, function (index, value) {
        value.volume = 1;
      });
      sound.game.volume = 0.4;
      sound.game2.volume = 0.15;
      sound.wrong.volume = 0.25;
      sound.clang.volume = 0.4;
      sound.special.volume = 0.8;
      sound.agent.volume = 1.0;
      $("#sound_off").hide();
      $("#sound_on").fadeIn(0);
    }
    if (game !== null) {
      game.audioOn = audioOn;
    } else if (tutorial !== null) {
      tutorial.audioOn = audioOn;
    }
  });

  /** click on ok button of dialog */
  $("#ok_iface").click(function () {
    if (game.activeDiag) {
      if (audioOn) {
        sound.button.currentTime = 0;
        sound.button.play();
      }
      $(iface).hide();
      game.activeDiag = false;
      game.scoreAdd = false;
      pauseGame();
    }
  });

  function tweetPage() {
    tw_score = finalScore;
    var tw_text = "Měl jsem skóre " + tw_score.toString() + "!";
    var tw_uri = "https://ridimeprahuspravne.cz";
    var tw_via = "PiratiStrana";
    window.open("https://twitter.com/share?url="
      + encodeURIComponent(tw_uri) + "&text=" + tw_text + "&via=" + tw_via, '',
      'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');
    return false;
  }

  function fbPage() {
    /** prepd for FB share */
    var fb_uri = "https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fridimeprahuspravne.cz&amp;src=sdkpreparse"
    
    window.open(fb_uri, '',
      'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=500,width=600');
    return false;
  }

  function piratiPage() {
    window.open("https://praha.pirati.cz/volby/2022-komunalni.html", "status=1,toolbar=1");
    return false;
  }

  $("#win_tw").click(function () {
    tweetPage()
  });

  $("#win_fb").click(function () {
    fbPage()
  });

  $("#win_program").click(function () {
    piratiPage();
  });

  $("#win_leaderboard").click(function () {
    showLeaderboard();
  });

  $("#leaderboard_join").click(async function () {
    const name = $("#leaderboard_nickname").val();
    
    if (name.length === 0) {
      alert("Prosím, zadej přezdívku.");
      return;
    }
    
    if (finalScore === 0) {
      alert("Musíš mít skóre aspoň 1.");
      return;
    }
    
    const response = await fetch(
      leaderboardURL,
      {
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        method: "POST",
        body: JSON.stringify({
          name: name,
          points: finalScore
        })
      }
    );
    
    if (!response.ok) {
      alert("Chyba při odesílání skóre.");
      return;
    }
    
    await loadLeaderboard();
    $("#leaderboard_nickname").val("");
  });

  $("#win_close").click(function () {
    var newWindow = window.open('', '_self', ''); //open the current window
    newWindow.close();
    // window.top.close();
  });

  /** show the you-win dialog */
  $("#vote_img").click(function () {
    if (audioOn && !voteCheckDiag) {
      sound.button.currentTime = 0;
      sound.button.play();
    }
    if (!voteCheckDiag) {
      voteCheckDiag = true
      duration = 1.0 * 1000;
      $("#cross").fadeIn(0).transition({ opacity: 0.0, visibility: "visible" }, 0).transition({ opacity: 1.0 }, duration).transition({ opacity: 0.0 }, duration)
      setTimeout(function () {
        $("#check").fadeIn(0).transition({ opacity: 0.0, visibility: "visible" }, 0).transition({ opacity: 1.0 }, duration)
        setTimeout(function () {
          $("#vote_ok_off").hide();
          $("#vote_ok_on").fadeIn(0);
          voteDiag = true;
        }, duration * 2);
      }, duration * 2);
    }
  });

  /** show the you-win dialog */
  $("#vote_ok_on").click(function () {
    if (audioOn) {
      sound.button.currentTime = 0;
      sound.button.play();
    }
    if (voteDiag) {
      voteDiag = false;
      youWin();
    }
  });

  /** start new game after win on click */
  $("#win_restart").click(function () {
    if (audioOn) {
      sound.button.currentTime = 0;
      sound.button.play();
    }
    restartGame();
  });

  /** Restart game */
  function restartGame() {
    setTimeout(function () {
      sound.game.pause();
      sound.game.currentTime = 0;
      sound.game2.pause();
      sound.game2.currentTime = 0;
      sound.agent.pause();
      sound.agent.currentTime = 0;
      $(".button_container").css({ visibility: "hidden" });
      $(".vote").hide();
      $(".vote").css({ visibility: "hidden" });
      $("#button_bg").css({ visibility: "hidden" });
      $(".score").hide();
      $(".time").hide();
      $(".win").css({ visibility: "hidden" });
      hideRemainingTargets();
      chosen();
      if (game !== null) {
        clearInterval(gameLoop);
        game = null;
      } else if (tutorial !== null) {
        endTutorial()
      }
      $("#start").fadeIn(500);
      $("#tutorial").fadeIn(500);
      $()
      readyToStart = true;
    }, 0);
    hideRemainingTargets();
  }

  /** start tutorial on click */
  $("#tutorial").click(function () {
    if (audioOn) {
      sound.button.currentTime = 0;
      sound.button.play();
    }
    if (readyToStart) {
      readyToStart = false;
      $("#tutorial").hide();
      $("#start").hide();
      startTutorial();
    }
  });

  /** start game on click */
  $("#start").click(function () {
    if (audioOn) {
      sound.button.currentTime = 0;
      sound.button.play();
    }
    if (readyToStart) {
      readyToStart = false;
      $("#start").hide();
      $("#tutorial").hide();
      startGame();
    }
  });

  /** click on interface */
  $(iface).click(function () {
    if (game.activeDiag) {
      if (audioOn) {
        sound.button.currentTime = 0;
        sound.button.play();
      }
      $(iface).hide();
      game.activeDiag = false;
      game.scoreAdd = false;
      pauseGame();
    }
  });

  /** do when the window changes its size */
  $(window).resize(function () {
    windowMinSize();
    if (tutorialActive) chosen(tutorialChosenButton);
    else chosen(chosenButton);

    if (winSize.y < winSize.x / (1920 / 1080)) {
      $(".mainBlock").css({ width: (1920 / 1080) * winSize.y + "px" });
    } else {
      $(".mainBlock").css({ width: 100 + "%" });
    }
  });

  /** calcs the size of window */
  function windowMinSize() {
    winSize.x = $(window).width();
    winSize.y = $(window).height();
    winSize.min = Math.min(winSize.x, winSize.y);

    $(".iface p").css({
      "font-size": winSize.x / 80 + "pt",
    });
  }

  /** fun for css chaining and activation of delay in chain */
  $.fn.extend({
    qcss: function (css) {
      return $(this).queue(function (next) {
        $(this).css(css);
        next();
      });
    },
  });

  /** main timed game loop */
  function gameLogic() {
    if (game !== null) {
      game.printTime();
      if (game.timeRemain <= 1) {
        endGame();
      }
    }
  }

  /** show the pre-win dialog */
  function youWin() {
    if (audioOn) {
      sound.win.currentTime = 0;
      sound.win.play();
    }
    $(".button_container").css({ visibility: "hidden" });
    $("#button_bg").css({ visibility: "hidden" });
    $(".vote").hide();
    $(".vote").css({ visibility: "hidden" });
    showWin();
    $(".score_time").hide();
    voteDiag = false;
    clearInterval(gameLoop);
    sound.game.pause();
    sound.game.currentTime = 0;
    sound.game2.pause();
    sound.game2.currentTime = 0;
  }

  /** End of game before win */
  function endGame() {
    finalScore = game.score;
    game.hideTargets();
    game = null;
    fadeoutSpeed = 500;
    $(".button_container").fadeOut(fadeoutSpeed);
    $("#button_bg").fadeOut(fadeoutSpeed);
    $(".iface").css({ visibility: "hidden" });
    $(".iface").hide();
    sound.game.pause();
    sound.game.currentTime = 0;
    sound.game2.pause();
    sound.game2.currentTime = 0;
    setTimeout(function () {
      showVote()
    }, 3000);
  }

  /** Blinking with button selection */
  function blink(time, interval) {
    blinkTimer = window.setInterval(function () {
      $("#button_bg").css("opacity", "0.1");
      window.setTimeout(function () {
        $("#button_bg").css("opacity", "1");
      }, 300);
    }, interval);
    window.setTimeout(function () {
      clearInterval(blinkTimer);
    }, time);
  }

  function endTutorial() {
    $(".note").css({opacity: 0.0});
    clearInterval(blinkTimer);
    tutorialActive = false;
    tutorial = null;
    fadeoutSpeed = 500;
    $(".button_container").fadeOut(fadeoutSpeed);
    // $("#button_bg").fadeOut(fadeoutSpeed);
    $("#button_bg").hide();
    $(".iface").css({ visibility: "hidden" });
    $(".iface").hide();
    sound.game.pause();
    sound.game.currentTime = 0;
    sound.game2.pause();
    sound.game2.currentTime = 0;
    $("#start").css({ visibility: "visible" });
    $("#start").hide();
    $("#start").fadeIn(fadeoutSpeed);
    $("#tutorial").fadeIn(fadeoutSpeed);
    readyToStart = true;
    chosen();
  }

  function hideRemainingTargets() {
    var target;
    for (var i = 1; i <= countTargets; i++) {
      if (i < 10) target = "#target0" + i;
      else target = "#target" + i;
      $(target).css({ visibility: "hidden" });
      $(target).hide();
    }
    $("#target_agent").hide();
  }

  /** Show dialog with vote action */
  function showVote() {
    hideRemainingTargets();

    // hide action button background
    chosen();

    // vote dialog
    voteDiag = true;
    $(".center.vote").css({ visibility: "visible" });
    $(".center.vote").fadeIn(0);
    $("#vote_ok_off").fadeIn(0);
    $("#vote_ok_on").hide();
    $("#cross").hide();
    $("#check").hide();
  }

  async function loadLeaderboard() {
    $("#loading").show();
    
    $("#leaderboard_list").html("");
    
    const response = await fetch(leaderboardURL);
    const json = await response.json();
    
    let position = 1;
    
    for (const result of json) {
      $("#leaderboard_list").html(
        $("#leaderboard_list").html()
        +
        `<li class="leaderboard-user">
        <div class="leaderboard-user-info">
        <div class="leaderboard-user-position">
        ${position}.
        </div>
        
        <div class="leaderboard-user-name">
        ${result.name}
        </div>
        </div>
        
        <div class="leaderboard-user-score">
        ${result.points}
        </div>
        </li>`
      );
      
      position++;
    }
    
    $("#loading").hide();
  }
  
  async function showLeaderboard() {
    // show dialog
    $(".center.leaderboard").css({ visibility: "visible" });
    $(".center.leaderboard").fadeIn(0);
    
    await loadLeaderboard();
  }
  
  function hideLeadeboard() {
    // hide dialog
    $(".center.leaderboard").css({ visibility: "hidden" });
    $(".center.leaderboard").fadeOut(0);
  }
  
  $("#leaderboard_close").click(
    function () {
      hideLeadeboard();
      showWin();
    }
  );
  
  /** Show dialog with vote action */
  function showWin() {
    // show dialog
    $(".center.win").css({ visibility: "visible" });
    $(".center.win").fadeIn(0);
    // show buttons
    $("#win_program").fadeIn(0);
    $("#win_restart").fadeIn(0);
    $("#win_fb").fadeIn(0);
    $("#win_tw").fadeIn(0);
    $("#win_close").fadeIn(0);
    // print final score
    $("#win_score").text(finalScore.toString().padStart(5, "0"));
  }

  function startTutorial() {
    if (blinkTimer === null) clearInterval(blinkTimer);
    hideRemainingTargets();
    tutorialActive = true;
    voteCheckDiag = false;
    voteDiag = false;
    // sound.waiting.pause();
    // show game elements
    setTimeout(function () {
      $(".button_container").css({ visibility: "visible" });
      $(".button_container").fadeIn(500);
      $(".iface").hide();
      if (audioOn) {
        sound.game.play();
        sound.game2.play();
      }
    }, 0);
    tutorialTargets = [];
    for (var i = 0; i < 3; i++) {
      tutorialTargets.push(new Target());
    }
    tutorialActive = true;
    tutorial = new Game(
      (timeTotal = endTime),
      (maxActiveTargets = noOfActiveTargets)
    );
    $("#note_general").transition({opacity: 1.0},500);
    $("#note_target01").transition({opacity: 1.0},500);
    tutorial.audioOn = audioOn;
    tutorial.specialActive = false;
    $("#pirati").css({ opacity: 0.5 });
    tutorialTargets[0].set(targetList[0]);
    tutorialChosenButton = "#lupa";
    moveBgButton(tutorialChosenButton);
    if (blinkTimer === null) blink(9e8, 1000);
  }

  function startGame() {
    if (blinkTimer === null) clearInterval(blinkTimer);
    hideRemainingTargets();
    tutorialActive = false;
    voteCheckDiag = false;
    voteDiag = false;
    // sound.waiting.pause();
    // show game elements
    setTimeout(function () {
      $(".button_container").css({ visibility: "visible" });
      $(".button_container").fadeIn(500);
      $(".score_time").css({ visibility: "visible" });
      $(".score_time").fadeIn(500);
      $(".score").css({ visibility: "visible" });
      $(".score").fadeIn(500);
      $(".time").css({ visibility: "visible" });
      $(".time").fadeIn(500);
      $(".iface").css({ visibility: "visible" });
      $(".iface").hide();
      if (audioOn) {
        sound.game.play();
        sound.game2.play();
      }
    }, 0);
    // endTime = 20;
    game = new Game(
      (timeTotal = endTime),
      (maxActiveTargets = noOfActiveTargets)
    );
    game.audioOn = audioOn;
    game.specialActive = false;
    $("#pirati").css({ opacity: 0.5 });
    game.printTime();
    game.printScore();
    chosen();
    if (typeof gameLoop != "undefined") clearInterval(gameLoop);
    gameLoop = setInterval(gameLogic, gameSpeed);
  }

  windowMinSize();
  chosen(chosenButton);
  if (winSize.y < winSize.x / (1920 / 1080)) {
    $(".mainBlock").css({ width: (1920 / 1080) * winSize.y + "px" });
  } else {
    $(".mainBlock").css({ width: 100 + "%" });
  }
  // sound.waiting.currentTime = 0;
  // sound.waiting.play();
  $("#sound_button").css({ visibility: "visible" });
  $("#fullscreen_button").css({ visibility: "visible" });
  $("#fullscreen_off").css({ visibility: "visible" });
  $("#fullscreen_on").css({ visibility: "visible" });
  $("#restart_button").css({ visibility: "visible" });
  $("#sound_on").css({ visibility: "visible" });
  $("#sound_off").css({ visibility: "visible" });
  $("#sound_off").hide();
  $("#sound_on").fadeIn(0);
  $("#fullscreen_off").hide();
  $("#fullscreen_on").fadeIn(0);
  $("#loading").hide();
  $("#start").fadeIn(0);
  $("#tutorial").fadeIn(0);
  $("#start").css({ visibility: "visible" });
  $("#tutorial").css({ visibility: "visible" });
  
  // https://stackoverflow.com/a/37559790
  // Thanks to Brock Adams!
  $(document).on(
    "keydown",
    function (zEvent) {
      if (zEvent.ctrlKey && zEvent.key === "ů") {  // case sensitive
        showWin();
      }
    }
  );
});
