class Target {
  /** create new target with its css object */
  constructor(x = 30, y = 50, z = 0, type = null, text = null) {
    this.x = x; // bottom
    this.y = y; // left
    this.z = z; // z-index
    this.type = type; // type of target
    this.text = text; // quote if target disappear
    this.spawntime = 0; // time when target appears
    this.lifetime = 0; // time when can be hit
    this.hidetime = this.spawntime + this.lifetime;
    this.id = document.querySelectorAll("#" + type); // jquery link
  }

  /** Function to generate random number */
  randomNumber(min, max) {
    var min = Math.ceil(min);
    var max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  animShow(duration = 1.0) {
    $(this.id)
      .delay(duration * 1000)
      .fadeIn(0)
      .qcss({ visibility: "visible" })
      .qcss({ animation: "jumpin " + duration + "s ease-in-out 0s 1" })
      .delay(duration * 1000);
  }

  /** set target according to targetItem from target_list */
  set(targetItem = new TargetItem(), activeTargets = new Array()) {
    this.type = targetItem.type;
    this.text = targetItem.text;
    this.id = document.querySelectorAll("img#" + this.type);
    this.layer = layerList[this.randomNumber(0, layerList.length - 1)];
    var width = 10;
    var coors = this.layer.randomCoors();
    // time parameters
    this.spawntime =  new Date().getTime() / 1000;
    this.lifetime = 15;
    this.hidetime = this.spawntime + this.lifetime;
    //check if free location
    if (activeTargets !== null) {
      for (var i = 0; i < activeTargets.length; i++) {
        // console.log(((activeTargets[i].id).offsetTop + $(activeTargets[i].id).outerHeight()));
        if (
          activeTargets[i].x == coors[0] &&
          activeTargets[i].y >= coors[1] - 1.5 * width &&
          activeTargets[i].y <= coors[1] + width
        ) {
          this.layer = layerList[this.randomNumber(0, layerList.length - 1)];
          coors = this.layer.randomCoors();
          i = 0;
        }
      }
    }
    this.move(coors[0], coors[1], coors[2]);
    this.animShow();
  }

  /** set coors of target to coors x,y */
  setXY(x, y, z) {
    this.x = x;
    this.y = y;
    this.z = z;
  }

  isold(currentTime) {
    if (currentTime > this.hidetime) return true;
    else return false;
  }

  /** move target to x and y (moves the css object too) */
  move(x, y, z) {
    this.setXY(x, y, z);
    $(this.id).css("bottom", this.x + "%");
    $(this.id).css("left", this.y + "%");
    $(this.id).css("z-index", this.z);
  }
}
