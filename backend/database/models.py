import datetime

import sqlalchemy

from . import Base
from .utils import UUID, get_uuid

from .. import utils

__all__ = ["Result"]


class Result(Base):
	"""A game score record."""

	__tablename__ = "results"

	id = sqlalchemy.Column(
		UUID,
		primary_key=True,
		default=get_uuid
	)

	name = sqlalchemy.Column(
		sqlalchemy.String(64),
		nullable=False
	)
	"""The nickname of the person who submitted the score."""

	points = sqlalchemy.Column(
		sqlalchemy.Integer,
		nullable=False
	)
	"""The amount of points the user got."""

	identifier = sqlalchemy.Column(
		sqlalchemy.String(64),
		default=utils.get_ip_hash,
		nullable=False
	)
	"""An identifier of the person who submitted the score. This should be, for
	example, a securely hashed IP address. As securely as IP addresses can be
	hashed, that is.
	"""
