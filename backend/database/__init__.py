import sqlalchemy.orm

Base = sqlalchemy.orm.declarative_base()

from .utils import UUID, get_uuid
from .models import Result

__all__ = [
	"UUID",
	"Result",
	"get_uuid"
]
