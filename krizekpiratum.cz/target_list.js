class Layer {
  constructor(bottom = 0, left_min = 0, left_max = 0, z = 0) {
    this.bottom = bottom;
    this.left_min = left_min;
    this.left_max = left_max;
    this.z = z;
  }

  /** Function to generate random number */
  randomNumber() {
    var min = Math.ceil(this.left_min);
    var max = Math.floor(this.left_max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  /** Coordinates for random target */
  randomCoors() {
    var randomLeft = this.randomNumber();
    return [this.bottom, randomLeft, this.z];
  }
}

var layerList = [
  new Layer(37, 10, 80, 800),
  new Layer(45, 13, 74, 600),
  new Layer(45, 20, 70, 400),
  new Layer(54, 23, 44, 200),
];

class TargetItem {
  constructor(type, text) {
    this.type = type;
    this.text = text;
  }
}

function str_pad_left(string, pad, length) {
  return (new Array(length + 1).join(pad) + string).slice(-length);
}

countTargets = 18;
var targetList = new Array(countTargets);
for (var i = 0; i < targetList.length; i++) {
  t_str = ("target" + str_pad_left(i + 1, "0", 2)).toString();
  targetList[i] = new TargetItem(
    (type = t_str),
    (text = ("Pirátské heslo #" + (i + 1)).toString())
  );
};

targetList[0].text = "Posvítíme si na kmotry a zloděje. Žádné vysávání městské kasy!";
targetList[6].text = "Žádné zavřené dveře na úřadě! Všechno bude vidět. Transparentnost!";
targetList[12].text = "Bojujeme proti korupční chobotnici na všech úrovních politiky!";
targetList[1].text = "Za čistý vzduch a zelené ulice! Pryč se smogem!";
targetList[7].text = "Máme odvahu postavit se za přírodu a za lidi. Stromy proti vedru!";
targetList[13].text = "Město jako poušť? Tak to teda ne! Chceme město zelenější!";
targetList[2].text = "Zastavíme vylidňování obcí, zvýšíme občanskou vybavenost!";
targetList[8].text = "Ceny bytů rostou. Je potřeba pečovat o městský bytový fond a stavět!";
targetList[14].text = "Žít pod mostem? To ne. Dostupné a důstojné bydlení pro všechny!";
targetList[3].text = "Utopit se pod tunou papíru? My říkáme, že budoucnost je digitalizace!";
targetList[9].text = "Diskety a papír jsou v propadlišti dějin. Digitalizujeme město!";
targetList[15].text = "Obíhat mají data, ne lidé! A úřady mají bezpečně mezi sebou sdílet dostupné informace.";
targetList[4].text = "Drahá elektřina?! Podpoříme rozvoj využívání obnovitelných zdrojů energie.";
targetList[10].text = "Drahý plyn?! Co takhle využívání bioplynek a obnovitelných zdrojů?";
targetList[16].text = "Stoupající ceny energií? Město bude maximálně využívat obnovitelné zdroje.";
targetList[5].text = "Chceme bezpečné a fajn prostředí pro všechny.";
targetList[11].text = "Zdá se vám, že se ve městě ničeho nedomůžete? My to tak nenecháme.";
targetList[17].text = "Myslíte si, že váš hlas nemá váhu proti jiným? Zapojíme lidi do rozhodování v obcích.";

agent_text = "Nenechme se otrávit populismem a lží.";
var agentItem = new TargetItem(type = "target_agent", text = agent_text);