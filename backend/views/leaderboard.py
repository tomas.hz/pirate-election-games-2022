import http.client
import typing

import flask
import sqlalchemy
import werkzeug.exceptions

from .. import database, utils, validators


leaderboard_blueprint = flask.Blueprint(
	"leaderboard",
	__name__
)


@leaderboard_blueprint.route("/", methods=["GET"])
def list_points() -> typing.Union[flask.Response, int]:
	scores = flask.g.sa_session.execute(
		sqlalchemy.select(database.Result.name, database.Result.points).
		order_by(sqlalchemy.desc(database.Result.points))
	).all()
	
	response = []
	
	for score in scores:
		response.append({
			"name": score[0],
			"points": score[1]
		})
	
	return flask.jsonify(response), http.client.OK


@leaderboard_blueprint.route("/", methods=["POST"])
@validators.validate_json({
	"name": {
		"type": "string",
		"minlength": 1,
		"maxlength": 16,
		"nullable": False
	},
	"points": {
		"type": "integer",
		"min": 1,
		"max": 150000,
		"nullable": False
	}
})
def submit_score() -> typing.Union[flask.Response, int]:
	remote_ip_hash = utils.get_ip_hash()
	
	existing_result = flask.g.sa_session.execute(
		sqlalchemy.select(database.Result).
		where(database.Result.identifier == remote_ip_hash)
	).scalars().one_or_none()
	
	if existing_result is None:
		flask.g.sa_session.add(
			database.Result(
				name=flask.g.json["name"],
				points=flask.g.json["points"],
				identifier=remote_ip_hash
			)
		)
	else:
		existing_result.name = flask.g.json["name"]
		existing_result.points = flask.g.json["points"]
	
	flask.g.sa_session.commit()
	
	return flask.jsonify(None), http.client.NO_CONTENT
