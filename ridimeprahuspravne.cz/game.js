class Game {
  constructor(
    timeTotal = 2.0 * 60,
    maxActiveTagets = 4,
    maxScore = 99999,
    nthQuote = 5,
    quoteDuration = 5,
    timeForSpecial = 29.0,
    specialEffect = 3
  ) {
    this.score = 0;
    this.maxScore = maxScore;
    this.timeTotal = timeTotal;
    this.timeStart = new Date().getTime() / 1000;
    this.currentTime = this.timeStart;
    this.timeRemainString = "";
    this.activeTargets = new Array();
    this.maxActiveTagets = maxActiveTagets;
    this.noActiveTagets = this.randomNumber(1, this.maxActiveTagets);
    this.specialActive = false;
    this.buttonList = new Array();
    this.nthQuote = nthQuote;
    this.quoteDuration = quoteDuration;
    this.timeForSpecial = timeForSpecial;
    this.specialEffect = specialEffect;
    this.audioOn = true;
    this.agentInCity = false;
    this.agentAlreadyShown = false;
    this.agentMin = 15.0;
    this.agentMax = 30.0;
    this.agentLoop = null;
    this.tAgent = null;
    this.nextAgentTime();

    // css stuff linking
    this.textEvent = document.querySelector("#textEvent");
    this.iface = document.querySelector(".iface");
    this.scoreText = document.querySelector("#score_text");
    this.timeText = document.querySelector("#time_text");
    this.special = document.querySelector("#pirati");

    for (var i = 0; i < $(".button").length; i++) {
      this.buttonList.push($(".button")[i].id);
    }
  }

  /** Use special action button and remove oldest targets */
  useSpecial() {
    this.specialEffect = this.activeTargets.length;
    for (var i = 0; i < this.specialEffect; i++) {
      if (this.activeTargets.length > 0) {
        this.addScore(this.activeTargets[0].type);
        $(this.activeTargets[0].id).fadeOut(4 * 500);
        this.removeTarget($(this.activeTargets[0]).attr("id"));
      } else {
        break;
      }
    }
    this.specialActive = false;
    $(this.special).css({ opacity: 0.5 });
  }

  /** check if the action button has effect on target  */
  isValidTarget(chosenTarget, chosenButton) {
    var noTarget = -1;
    var mod = -1;
    var indexButton = -1;
    noTarget = $(chosenTarget)
      .attr("id")
      .substring($(chosenTarget).attr("id").length - 2);
    mod = (noTarget - 1) % 6;
    indexButton = this.buttonList.indexOf($(chosenButton).attr("id"));

    if (mod == indexButton) {
      return true;
    } else {
      return false;
    }
  }

  /** Add score and update text accordingly */
  addScore(targetID) {
    var value = 0;
    if (targetID.localeCompare("target_agent") == 0) {
      value = Math.floor(
        300 *
          ((this.currentTime - this.timeStart) /
            (this.tAgent.hidetime - this.timeStart))
      );
    } else {
      for (var i = 0; i < this.activeTargets.length; i++) {
        if (
          targetID.localeCompare($(this.activeTargets[i].id).attr("id")) == 0
        ) {
          value = Math.floor(
            100 *
              ((this.currentTime - this.timeStart) /
                (this.activeTargets[i].hidetime - this.timeStart))
          );
          break;
        }
      }
    }

    if (value < 0) value = 0;
    if (this.score + value < this.maxScore) {
      this.score += value;
      this.printScore();
    }
  }

  /** show the quest dialog */
  showQuest(chal) {
    if (chal != null) {
      var fadeTime = 500;
      $(this.textEvent).text(chal.text);
      $(this.iface)
        .fadeIn(fadeTime)
        .delay(this.quoteDuration * 1000)
        .fadeOut(fadeTime);
    }
  }

  /** Hide target by id */
  hideTargets() {
    var target;
    for (var i = 1; i <= countTargets; i++) {
      if (i < 10) target = "#target0" + i;
      else target = "#target" + i;
      $(target).css({ visibility: "hidden" });
      $(target).hide();
    }
    $("#target_agent").hide();
    this.endAgentRaid();
  }

  /** Remove target by id */
  removeTarget(target_id) {
    for (var i = 0; i < this.activeTargets.length; i++) {
      if (
        $(target_id)
          .attr("id")
          .localeCompare($(this.activeTargets[i].id).attr("id")) == 0
      ) {
        this.activeTargets.splice(i, 1);
        break;
      }
    }
    this.noActiveTagets = this.randomNumber(2, this.maxActiveTagets);
  }

  /** Function to generate random number */
  randomNumber(min, max) {
    var min = Math.ceil(min);
    var max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  agentMovement(tAgent, agentSpeed, flipSpeed) {
    $(tAgent.id)
      .transition(
        {
          left: tAgent.layer.left_min + "%",
        },
        agentSpeed
      )
      .transition(
        {
          rotateY: "180deg",
        },
        flipSpeed
      )
      .transition(
        {
          left: tAgent.layer.left_max + "%",
        },
        agentSpeed
      )
      .transition(
        {
          rotateY: "0deg",
        },
        flipSpeed
      );
  }

  /** Raid has started, Agent is in the city */
  agentRaid() {
    var agentSpeed = 4.0 * 1000;
    var flipSpeed = 1.0 * 1000;
    this.agentInCity = true;
    this.tAgent = new Target();
    this.tAgent.set(agentItem, null);
    this.tAgent.lives = 3;
    sound.agent.currentTime = 0;
    sound.agent.play();
    if (this.audioOn) {
      sound.agent.volume = 0.5;
    } else {
      sound.agent.volume = 0.0;
    }
    this.agentMovement(this.tAgent, agentSpeed, flipSpeed);
    this.agentLoop = setInterval(
      this.agentMovement,
      2 * agentSpeed + 2 * flipSpeed,
      this.tAgent,
      agentSpeed,
      flipSpeed
    );
  }

  nextAgentTime() {
    this.agentTime = Math.floor(
      this.randomNumber(this.agentMin, this.agentMax) +
        new Date().getTime() / 1000
    );
  }

  endAgentRaid() {
    sound.agent.pause();
    if (typeof this.agentLoop != "undefined") clearInterval(this.agentLoop);
    this.agentInCity = false;
    this.nextAgentTime();
    if (this.tAgent.randomNumber(0, this.nthQuote) == this.nthQuote)
      this.showQuest(this.tAgent);
  }

  /** Get random target */
  randomTarget() {
    var randFromList = null;
    randFromList = targetList[this.randomNumber(0, countTargets - 1)];
    if (this.activeTargets.length > 0) {
      for (var i = 0; i < this.activeTargets.length; i++) {
        if (this.activeTargets[i].type.localeCompare(randFromList.type) == 0) {
          randFromList = targetList[this.randomNumber(0, countTargets - 1)];
          i = 0;
        }
      }
    }
    var t = new Target();
    t.set(randFromList, this.activeTargets);
    this.activeTargets.push(t);
    if (t.randomNumber(0, this.nthQuote) == this.nthQuote) this.showQuest(t);
  }

  /** Update score in web page */
  printScore() {
    $(this.scoreText).text(this.score.toString().padStart(5, "0"));
  }

  /** Formatting string with padding */
  str_pad_left(string, pad, length) {
    return (new Array(length + 1).join(pad) + string).slice(-length);
  }

  hideOldTarget(target) {
    $(target.id).stop();
    $(target.id)
      .transition(
        {
          bottom: 0 + "%",
        },
        1000
      )
      .hide(0);
  }

  checkTargetLifetime() {
    for (var i = 0; i < this.activeTargets.length; i++) {
      if (this.activeTargets[i].isold(this.currentTime)) {
        this.hideOldTarget(this.activeTargets[i]);
        this.activeTargets.splice(i, 1);
        i = 0;
      }
    }
  }

  /** Update time in web page */
  printTime() {
    // calc remaining time
    this.currentTime = new Date().getTime() / 1000;
    this.timeRemain = Math.floor(
      this.timeTotal - (this.currentTime - this.timeStart)
    );
    var minutes = Math.floor(this.timeRemain / 60);
    var seconds = Math.floor(this.timeRemain - minutes * 60);

    // print time
    this.timeRemainString = (
      this.str_pad_left(minutes, "0", 2) +
      ":" +
      this.str_pad_left(seconds, "0", 2)
    ).toString();
    $(this.timeText).text(this.timeRemainString);

    // check lifetime
    this.checkTargetLifetime();

    // gen new target
    if (this.activeTargets.length < this.noActiveTagets) {
      this.randomTarget();
    }
    // activate special button
    if (
      (this.timeRemain - this.timeTotal) % this.timeForSpecial === 0 &&
      this.timeRemain != this.timeTotal
    ) {
      this.specialActive = true;
      $(this.special).css({ opacity: 1.0 });
    }
    
    // start Agent raid
    if (!this.agentInCity && this.agentTime - new Date().getTime() / 1000 < 0) {
      if (this.agentAlreadyShown) sound.agent.setAttribute(
        'src',
        'sound/agent.mp3'
      );
      
      this.agentAlreadyShown = true;

      this.agentRaid();
    }
  }
}
